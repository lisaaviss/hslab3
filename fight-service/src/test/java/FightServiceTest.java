import config.CustomPostgreSQLContainer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@SpringBootTest(classes=FightService.class)
@ExtendWith(MockitoExtension.class)
public class FightServiceTest extends CustomPostgreSQLContainer {
    @Mock
    FightService fightService;

    @Mock
    private StandRestConsumer standRepository;

    @Test
    public void fightStartsTest() {
//        when(fightService.startFight(1L)).thenReturn(new StandDto(1L, 1d, 1d, 1d, 1d, 1d, 1d));
        assertNotNull(fightService.startFight(1L));
    }

    @Test
    public void findAllTest() {
        when(fightService.findAll(PageRequest.of(1,1))).thenReturn(Page.empty());
        assertEquals(fightService.findAll(PageRequest.of(1,1)).stream().toList().size(), 0);
    }

//    @Test
//    public void repoTest() {
//        when(standRepository.getStandById(1L)).thenReturn(Optional.empty());
//        assertEquals(Optional.empty(), standRepository.getStandById(1L));
//    }
}
