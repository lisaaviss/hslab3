package com.example.gogo2.fight.repository;

import com.example.gogo2.fight.model.entity.Library;
import org.springframework.data.repository.CrudRepository;

public interface LibraryRepo extends CrudRepository<Library, Long> {

}
