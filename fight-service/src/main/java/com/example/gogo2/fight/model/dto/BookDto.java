package com.example.gogo2.fight.model.dto;



import com.example.gogo2.fight.model.entity.Book;

import java.util.List;

public class BookDto {
    private Long id;
    private String name;
    private String author;

    private Long library_id;

    private List<Long> orders;


    public static BookDto toDto(Book entity){
        BookDto dto = new BookDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAuthor(entity.getAuthor());
        dto.setLibrary_id(entity.getLibrary().getId());
        return dto;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Long getLibrary_id() {
        return library_id;
    }

    public void setLibrary_id(Long library_id) {
        this.library_id = library_id;
    }
}
