package com.example.gogo2.fight.service;


import com.example.gogo2.fight.model.dto.*;
import com.example.gogo2.fight.model.entity.Book;
import com.example.gogo2.fight.model.entity.Order;
import com.example.gogo2.fight.repository.BookRepo;
import com.example.gogo2.fight.repository.LibraryRepo;
import com.example.gogo2.fight.repository.OrderRepo;
import com.example.gogo2.fight.repository.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class OrderService {
    private final OrderRepo orderRepo;
    private final UserRepo readerRepo;
    private final LibraryRepo libraryRepo;
    private final BookRepo bookRepo;

    public Order createOrder(OrderDto orderDto) {
        Order order = new Order();

        order.setReader(readerRepo.findById(orderDto.getReader_id()).orElse(null));
        order.setLibrary(libraryRepo.findById(orderDto.getLibrary_id()).orElse(null));

        // Создаем список книг для заказа
        List<Book> books = new ArrayList<>();
        for (Long bookId : orderDto.getBooks()) {
            Book book = bookRepo.findById(bookId).orElse(null);
            if (book != null) {
                books.add(book);
            }
        }

        // Устанавливаем связь многие ко многим
        order.setBooks(books);

        return orderRepo.save(order);
    }

    public OrderDtoShow showOrder(Long orderId) {
        Order entity = orderRepo.findById(orderId).orElse(null);
        OrderDtoShow dto = new OrderDtoShow();
        dto.setId(entity.getId());
        dto.setReader(UserDto.toDto(entity.getReader()));
        dto.setLibrary(LibraryDto.toDto(entity.getLibrary()));
        dto.setBooks(entity.getBooks().stream()
                .map(BookDto::toDto)
                .collect(Collectors.toList())
        );
        return dto;

    }
}
