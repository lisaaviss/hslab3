package com.example.gogo2.fight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class FightServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(FightServiceApp.class, args);
    }
}
