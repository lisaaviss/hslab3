package com.example.gogo2.fight.model.dto;

import com.example.gogo2.fight.model.entity.User;
import lombok.Data;

@Data
public class UserDto {
    private Long id;
    private String name;
    private Long role_id;

    public UserDto(){

    }

    public static UserDto toDto(User entity){
        UserDto dto = new UserDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }

}
