package com.example.gogo2.fight.repository;

import com.example.gogo2.fight.model.entity.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepo extends CrudRepository<Book, Long> {

}
