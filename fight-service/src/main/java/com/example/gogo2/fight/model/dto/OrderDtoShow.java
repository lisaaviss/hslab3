package com.example.gogo2.fight.model.dto;

import java.util.List;

public class OrderDtoShow {
    private Long id;
    private UserDto reader;

    private LibraryDto library;

    private List<BookDto> books;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserDto getReader() {
        return reader;
    }

    public void setReader(UserDto reader) {
        this.reader = reader;
    }

    public LibraryDto getLibrary() {
        return library;
    }

    public void setLibrary(LibraryDto library) {
        this.library = library;
    }

    public List<BookDto> getBooks() {
        return books;
    }

    public void setBooks(List<BookDto> books) {
        this.books = books;
    }
}
