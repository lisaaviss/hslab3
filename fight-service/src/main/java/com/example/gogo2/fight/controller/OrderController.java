package com.example.gogo2.fight.controller;


import com.example.gogo2.fight.config.ApiConstants;
import com.example.gogo2.fight.model.dto.OrderDto;
import com.example.gogo2.fight.model.dto.OrderDtoShow;
import com.example.gogo2.fight.model.entity.Order;
import com.example.gogo2.fight.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(ApiConstants.BASE_API_PATH + "/order")
public class OrderController {
    private final OrderService orderService;

    @PostMapping("/make")
    public ResponseEntity<String> createOrder(@RequestBody OrderDto orderDto) {
        Order createdOrder = orderService.createOrder(orderDto);
        return ResponseEntity.ok("Order created with ID: " + createdOrder.getId());
    }

    @GetMapping("/show/{orderId}")
    public ResponseEntity showOrder(@PathVariable Long orderId){
        OrderDtoShow orderDto = orderService.showOrder(orderId);
        return ResponseEntity.ok(orderDto);
    }
}
