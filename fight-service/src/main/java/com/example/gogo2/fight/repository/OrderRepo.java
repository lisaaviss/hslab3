package com.example.gogo2.fight.repository;


import com.example.gogo2.fight.model.entity.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepo extends CrudRepository<Order, Long> {

}
