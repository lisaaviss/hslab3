CREATE TABLE orders (
                        id SERIAL PRIMARY KEY,
                        user_id BIGINT,
                        library_id BIGINT,
                        FOREIGN KEY (user_id) REFERENCES users(id),
                        FOREIGN KEY (library_id) REFERENCES libraries(id)
);