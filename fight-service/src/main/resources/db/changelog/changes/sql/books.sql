CREATE TABLE books (
                       id SERIAL PRIMARY KEY,
                       name VARCHAR(255),
                       author VARCHAR(255),
                       library_id BIGINT,
                       FOREIGN KEY (library_id) REFERENCES libraries(id)
);
