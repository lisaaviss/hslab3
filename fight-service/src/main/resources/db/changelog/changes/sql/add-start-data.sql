insert into status(id, name) values
                                 (1, 'default'), (2, 'vip'), (3, 'admin');
insert into users(name, password, role_id) values
    ('admin', '$2a$10$QmCI9k3e4nv6g1/HRIETcOwg6DhnawETCSPLYhXLGDseZ1.Y0NWIm', 3);
insert into libraries(name) values ('Michailovksaya');
insert into books(name, author, library_id) values ('Демон', 'Михаил Лермонтов', 1);

