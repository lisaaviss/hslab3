CREATE TABLE books_orders (
                              order_id BIGINT,
                              book_id BIGINT,
                              PRIMARY KEY (order_id, book_id),
                              FOREIGN KEY (order_id) REFERENCES orders(id),
                              FOREIGN KEY (book_id) REFERENCES books(id)
);