package com.example.userservice.security.filter;


import com.example.userservice.model.entity.Status;
import com.example.userservice.model.entity.User;
import com.example.userservice.repository.StatusRepository;
import com.example.userservice.security.util.JwtTokenUtil;
import com.example.userservice.service.UserService;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

import static org.springframework.util.StringUtils.hasText;

@Component
@RequiredArgsConstructor
public class JwtFilter extends GenericFilterBean {
    private static final String AUTHORIZATION = "Authorization";
    private final UserService userService;
    private final CircuitBreaker circuitBreaker;
    private final JwtTokenUtil jwtTokenUtil;
    private final StatusRepository statusRepository;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String token = getTokenFromRequest((HttpServletRequest) servletRequest);
        if (token != null && validateToken(token)) {
            String userLogin = getLoginFromToken(token);
            User user = userService.findUserByName(userLogin).block();
            Status status = statusRepository.findStatusById(user.getRole_id()).block();
            user.setStatus(status);
            UserDetails account = user;
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(account, null, account.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(auth);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private boolean validateToken(String token) {
        return circuitBreaker.decorateSupplier(()->(boolean) jwtTokenUtil.validateToken(token)).get();
    }

    private String getLoginFromToken(String token) {
        return circuitBreaker.decorateSupplier(() -> jwtTokenUtil.getLoginFromToken(token)).get();
    }

    private String getTokenFromRequest(HttpServletRequest request) {
        String bearer = request.getHeader(AUTHORIZATION);
        if (hasText(bearer) && bearer.startsWith("Bearer ")) {
            return bearer.substring(7);
        }
        return null;
    }
}

