package com.example.userservice.security.config;

import com.example.userservice.security.filter.JwtFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Service;

import java.util.Map;

@Configuration
@Service
@RequiredArgsConstructor
public class SecurityConfig {
    private final Map<String, String[]> patternMap = Map.of(
            "admin", new String[] {
                    "api/v1/gogo/register",
            },
            "all", new String[] {
                    "api/v1/gogo/login",
                    "/api/v1/library/order/make",
                    "api/v1/gogo/user/**",
                    "api/v1/gogo/user/linkstand",
                    "api/v1/gogo/stand/create",
                    "api/v1/gogo/stand/**"
            }
    );

    private final JwtFilter jwtFilter;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .authorizeRequests()
                                .requestMatchers(patternMap.get("admin")).hasAuthority("admin")
                                .requestMatchers(patternMap.get("all")).permitAll()

                .and()
                .csrf().disable()
                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class)
                .build();

    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
