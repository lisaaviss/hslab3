package com.example.userservice.controller;


import com.example.userservice.config.ApiConstants;
import com.example.userservice.model.dto.UserDtoForLogin;
import com.example.userservice.security.util.JwtTokenUtil;
import com.example.userservice.service.LoginService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//полгода плохая погода

@Tag(name = "Controller for login logic")
@RestController
@RequestMapping(ApiConstants.BASE_API_PATH)
@AllArgsConstructor
public class LoginController {

    private final LoginService loginService;
    private final JwtTokenUtil jwtTokenUtil;

    @PostMapping("login")
    @Operation(description = "login into system",
            summary = "login into system",
            tags = "client")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "логин удался"),
            @ApiResponse(responseCode = "403", description = "no right to access")
    })
    public ResponseEntity<String> login(@RequestBody UserDtoForLogin userDtoForLogin) {
        return ResponseEntity.ok(loginService.login(userDtoForLogin));
    }

    @PostMapping("validate")
    @Operation(description = "validate existing token",
            summary = "token validation",
            tags = "token")
    @ApiResponse(responseCode = "200", description = "validation success")
    public ResponseEntity<Boolean> validateToken(@RequestBody String token) {
        return ResponseEntity.ok(jwtTokenUtil.validateToken(token));
    }

    @PostMapping("get-login-from-token")
    @Operation(description = "get login from token",
            summary = "login string from token string",
            tags = "token")
    @ApiResponse(responseCode = "200", description = "success")
    public ResponseEntity<String> getLoginFromToken(@RequestBody String token) {
        return ResponseEntity.ok(jwtTokenUtil.getLoginFromToken(token));
    }
}
