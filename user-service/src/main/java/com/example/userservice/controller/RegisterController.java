package com.example.userservice.controller;


import com.example.userservice.config.ApiConstants;
import com.example.userservice.model.dto.UserDtoForRegister;
import com.example.userservice.model.entity.User;
import com.example.userservice.service.RegisterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(ApiConstants.BASE_API_PATH)
@AllArgsConstructor
public class RegisterController {

    private RegisterService registerService;

    @PostMapping("register")
    @Operation(description = "register",
            summary = "register a new user, admin only",
            tags = "register")
    @ApiResponse(responseCode = "200", description = "register success")
    public Mono<User> register(@RequestBody UserDtoForRegister userDtoForRegister) {
        return registerService.register(userDtoForRegister);
    }
}
