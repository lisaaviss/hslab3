package com.example.userservice.controller;


import com.example.userservice.model.entity.Status;
import com.example.userservice.service.StatusService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/status")
@RequiredArgsConstructor
public class StatusController {
    private final StatusService statusService;

    @GetMapping("/{name}")
    @Operation(description = "get status by name",
            summary = "get a status",
            tags = "status")
    @ApiResponse(responseCode = "200", description = "ok")
    public Flux<ResponseEntity<Status>> getStatus(@PathVariable String name) {
        return statusService.findStatusByName(name)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
