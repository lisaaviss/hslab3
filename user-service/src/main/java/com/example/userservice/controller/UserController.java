package com.example.userservice.controller;


import com.example.userservice.config.ApiConstants;
import com.example.userservice.model.entity.User;
import com.example.userservice.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(ApiConstants.BASE_API_PATH + "/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/{id}")
    @Operation(description = "get user by id",
            summary = "get user by id if exists",
            tags = "user")
    @ApiResponse(responseCode = "200", description = "user found")
    public Mono<ResponseEntity<User>> getUserById(@PathVariable long id) {
        return userService.findUserById(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
