package com.example.userservice.repository;


import com.example.userservice.model.entity.Status;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface StatusRepository extends ReactiveCrudRepository<Status, Long> {
    @Query(value = "select * from status where name = :name")
    Flux<Status> findStatusByName(@Param("name") String name);

    @Query(value = "select * from status where id = :id")
    Mono<Status> findStatusById(@Param("id") Long id);
}
