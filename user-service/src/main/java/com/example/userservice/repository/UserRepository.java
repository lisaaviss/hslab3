package com.example.userservice.repository;

import com.example.userservice.model.entity.User;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;


@Repository
public interface UserRepository extends ReactiveCrudRepository<User, Long> {
    @Query(value = "select * from users where name = :name")
    Mono<User> findUserByName(@Param("name") String name);

    @Query(value = "select * from users where id = :id")
    Mono<User> findUserById(@Param("id") Long id);

    @Query(value = "insert into users (name, password, role_id)" +
            "values (:name, :password, :role_id)")
    Mono<User> save(@Param("name") String name, @Param("password") String password, @Param("role_id") Long role_id);
}
