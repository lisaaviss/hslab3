package com.example.userservice.service;


import com.example.userservice.model.entity.Status;
import com.example.userservice.model.entity.User;
import com.example.userservice.repository.StatusRepository;
import com.example.userservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final StatusRepository statusRepository;

    public Mono<User> findUserByName(String name) {
        return (name != null) ? userRepository.findUserByName(name) : null;
    }

    public Mono<User> findUserById(long id) {
        User user = userRepository.findUserById(id).block();
        Status status = statusRepository.findStatusById(user.getRole_id()).block();
        user.setStatus(status);
        user.setPassword("");
        return Mono.just(user);
    }

}
