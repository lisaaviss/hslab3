package com.example.userservice.service;

import com.example.userservice.model.dto.UserDtoForRegister;
import com.example.userservice.model.entity.Status;
import com.example.userservice.model.entity.User;
import com.example.userservice.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class RegisterService {

    private UserRepository userRepository;
    private StatusService statusService;
    private PasswordEncoder passwordEncoder;


    public Mono<User> register(UserDtoForRegister userDto) {
        Status status = statusService.findStatusByName(userDto.getStatusName()).blockFirst();
        if (userRepository.findUserByName(userDto.getName()).block() != null) {

        }
        if (userDto.getPassword().length() < 5) {

        }

        return userRepository.save(userDto.getName(), passwordEncoder.encode(userDto.getPassword()), status.getId());
    }
}
