package com.example.userservice.service;

import com.example.userservice.model.entity.Status;
import com.example.userservice.repository.StatusRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
@AllArgsConstructor
public class StatusService {
    private StatusRepository statusRepository;

    public Flux<Status> findStatusByName(String name) {
        return this.statusRepository.findStatusByName(name);
    }
}
