package com.example.userservice.service;


import com.example.userservice.model.dto.UserDtoForLogin;
import com.example.userservice.model.entity.User;
import com.example.userservice.security.util.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class LoginService {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenUtil jwtTokenUtil;

    public String login(UserDtoForLogin userDtoForLogin) {
        User user = userService.findUserByName(userDtoForLogin.getName()).block();
        if (user == null || !passwordEncoder.matches(userDtoForLogin.getPassword(), user.getPassword()));
        return jwtTokenUtil.generateAccessToken(userDtoForLogin.getName());
    }
}

