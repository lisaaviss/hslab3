package com.example.userservice.model.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class UserDtoForLogin {
    private String name;
    private String password;
}
