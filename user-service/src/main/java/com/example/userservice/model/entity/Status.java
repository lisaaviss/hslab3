package com.example.userservice.model.entity;

import lombok.*;
import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Status {
    @Id
    private Long id;

    @NonNull
    private String name;
}
