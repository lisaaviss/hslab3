package com.example.hslab3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hslab3Application {

	public static void main(String[] args) {
		SpringApplication.run(Hslab3Application.class, args);
	}

}
